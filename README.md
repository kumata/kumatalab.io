# Kumata's blog

- https://www.kumata.club
- https://kumata.gitlab.io


## Deploy

1. hexo install

```
mkdir hexo-tool && cd hexo-tool

npm install -g hexo-cli
```

add `hexo-tool/bin/hexo` to path
```
export PATH=[location]/hexo-tool/bin:$PATH
```




2. Clone blog's hexo branch

```
git clone https://gitlab.com/kumata/kumata.gitlab.io

```


3. 项目文件夹下npm install tools

```
npm install hexo-deployer-git
```


4. 重新配置gitlab和coding的公钥

```
git config --global user.name "[your_gitlab_name]"
git config --global user.email "[your_gitlab_email]"

// my infos
git config --global user.name "kumata"
git config --global user.email "153236205@qq.com"
```

## Usage

- add blog .md

```
hexo new post "xxx"
```

1. hexo debug

- script debug usage

```
./debug.sh
```


- or step by step

```

// delete db: dir "public"
hexo clean

// generate
hexo g

// localhost:4000
hexo s
```



2. push to gitlab


- script usage

```
./upload.sh [commit infos]
```


- or push step by step 

```
hexo clean

hexo g

hexo d

git add -A

git commit -m "update"

git push
```








