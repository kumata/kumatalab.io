---
title: 以太坊(Ethereum)账户地址的生成过程
top: false
cover: false
toc: true
mathjax: true
date: 2019-3-24 20:16:32
password:
summary: 以太坊钱包(客户端)如何生成账户地址的？和比特币有什么不同？
tags:
    - crypto
    - Ethereum
categories:
    - 区块链
---


对比上一篇`比特币(BTC)账户地址的生成过程`，现在再总结对比一下以太坊中地址生成的不同之处。


# 私钥、公钥和地址是如何生成的？

以太坊地址生成的流程也是：私钥 -> 公钥 -> 地址。因此地址的生成需要三步：

- 生成一个随机的私钥（32字节）
- 通过私钥生成公钥（64字节）
- 通过公钥得到地址（20字节）


# 以太坊地址生成的过程

### 第一步：私钥 (private key)

伪随机数产生的256bit私钥示例(256bit  16进制32字节)
```
18e14a7b6a307f426a94f8114701e7c8e774e7f9a47e2c2035db29a206321725
```


### 第二步：公钥 (public key)

1. 采用椭圆曲线数字签名算法ECDSA-secp256k1将私钥（32字节）映射成公钥（65字节）（前缀04+X公钥+Y公钥）：

```
04
50863ad64a87ae8a2fe83c1af1a8403cb53f53e486d8511dad8a04887e5b2352
2cd470243453a299fa9e77237716103abc11a1df38855ed6f2ee187e9c582ba6
```
    
2. 拿公钥（非压缩公钥）来hash，计算公钥的 Keccak-256 哈希值（32bytes）：
```
fc12ad814631ba689f7abe671016f75c54c607f082ae6b0881fac0abeda21781
```

3. 取上一步结果取后20bytes即以太坊地址：
```
1016f75c54c607f082ae6b0881fac0abeda21781
```

### 第三步：地址 (address)

```
0x1016f75c54c607f082ae6b0881fac0abeda21781
```


# 如何生成一个私钥

私钥是一组64位的16进制字符，通过私钥我们能够访问一个账户。以太坊的私钥生成是通过secp256k1^5曲线生成的，secp256k1是一个椭圆曲线算法，比特币使用的也是相同的曲线算法。

通过Linux下OpenSSL工具我们可以生成一个椭圆曲线私钥：

```
$ openssl ecparam -name secp256k1 -genkey -noout                                                                                                               
-----BEGIN EC PRIVATE KEY-----
MHQCAQEEICGlTPPQInj0R/jaa7+bjF1twiR3RDLdOChSq98L5FmWoAcGBSuBBAAK
oUQDQgAERynScthXq2n4Ahkfp08s/QNogZEtVCfQE/XTvpjsnIeQEZGJIOb+Liyl
uF8PIerBE1CjvCs5LLU+fZz+B31+Bg==
-----END EC PRIVATE KEY-----
```



# 如何用私钥推导生成公钥

私钥是一组64位的16进制字符，通过私钥我们能够访问一个账户。以太坊的私钥生成是通过secp256k1椭圆曲线算法生成的，secp256k1是一个椭圆曲线算法，同比特币。

其实，通过OpenSSL我们可以同时得到私钥和公钥：

```
$ openssl ecparam -name secp256k1 -genkey -noout | openssl ec -text -noout                                                                                    
read EC key
Private-Key: (256 bit)
priv:
    3f:64:bb:20:0a:b5:82:e9:73:03:8a:8b:79:68:62:
    41:8b:98:a7:10:00:fb:50:de:c4:4d:0d:06:3d:a2:
    ed:cd
pub:
    04:4a:18:c2:c7:40:f4:9a:77:b2:89:e9:27:0c:39:
    94:8b:94:10:a1:b0:c9:81:d9:af:06:8c:06:23:93:
    63:d7:26:82:fd:b0:22:fe:f6:7f:4f:8a:69:58:2f:
    98:3a:b3:94:ab:5f:06:85:4c:25:f3:3d:8e:f1:35:
    2f:e7:fe:50:4d
ASN1 OID: secp256k1
```



# 如何用公钥推导生成地址

和比特币相比，在私钥生成公钥这一步其实是一样的，区别在公钥推导地址第一部分，以太坊中非圧缩型公钥的处理就简单粗暴很多了，下图为Public Key生成Address的过程。


```
$ openssl ecparam -name secp256k1 -genkey -noout | openssl ec -text -noout            

read EC key
Private-Key: (256 bit)
priv:
    3f:64:bb:20:0a:b5:82:e9:73:03:8a:8b:79:68:62:
    41:8b:98:a7:10:00:fb:50:de:c4:4d:0d:06:3d:a2:
    ed:cd
pub:
    04:4a:18:c2:c7:40:f4:9a:77:b2:89:e9:27:0c:39:
    94:8b:94:10:a1:b0:c9:81:d9:af:06:8c:06:23:93:
    63:d7:26:82:fd:b0:22:fe:f6:7f:4f:8a:69:58:2f:
    98:3a:b3:94:ab:5f:06:85:4c:25:f3:3d:8e:f1:35:
    2f:e7:fe:50:4d
ASN1 OID: secp256k1
```

地址是通过对上述的公钥做Keccak-256哈希，然后取最后的40位16进制字符得到的。我们对上述的公钥做哈希后并取后40位的结果是：`0x24602722816b6cad0e143ce9fabf31f6026ec622`。得到的该结果就是一个有效的以太坊地址。



一般以太坊钱包客户端都会自动帮我们完成如上步骤，了解原理之后就放心且舒爽了。


<p align="right">End.</p>
