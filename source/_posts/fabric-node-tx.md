---
title: Hyperledger Fabric 节点类型与交易流程
top: false
cover: false
toc: true
mathjax: true
date: 2020-02-26 19:43:16
password:
summary: 初步摸索HyperledgerFabric的节点和运作流程是怎么个情况。
tags:
    - Fabric
categories:
    - 区块链
---


# 节点类型

1. 客户端(节点)

应用程序、SDK、命令行
不算链的底层节点，负责连接Peer节点和程序，发生交易。

2. CA证书(可选)

负责对网络中所有的证书进行管理，提供标准的PKI服务；只有被ca认可的身份才能在链里交易。
 
3. Fabric Peer 背书节点/记账节点

Peer节点类型不冲突，一种节点可能包含如下三种功能：
- Endorser 背书节点：和Contract绑定，为交易的提案(proposal)检查和背书，计算交易执行的结果，
- Anchor 主节点：从Orderer节点获取信息，保存区块更新世界状态，
- Committer 记账节点/确认节点：所有的Peer都是记账节点，在接受交易结果前再次检查合法性，接受合法交易对账本的修改，并写入区块链结构。

4. Orderer 排序节点
主网接受交易并排序 (共识类型: solo，kafKa，PBFT)。
对所有发往网络的交易进行排序，将排序后的交易按照配置中的约定整理为区块之后提交给确认节点进行处理。
 
# 交易流程
整个过程总结为三个部分：

1. 交易模拟：智能合约部分，涉及背书节点签名
2. 交易排序：共识机制部分
3. 交易同步：各节点记账，账本存储部分
 
以下图五个步骤进行分析：

![Fabric交易流程](1.png)