---
title: Filecoin:简要分析挖矿流程与软硬件分配
top: false
cover: false
toc: true
mathjax: true
date: 2020-08-26 14:04:04
password:
summary: 本篇梳理一下基于IPFS&Filecoin的挖矿流程及基本的软硬件配置情况。
tags:
- filecoin
- 挖矿
categories:
- 区块链
---

Update on 2020.10.28 ..


# 前言

IPFS以及FileCoin的白皮书知识量比较大，需要花费一些时间成本来学习。
下面先针对挖矿相关的知识进行研究，总结一下Filecoin中挖矿的流程以及相关概念。

# What is Filecoin

IPFS（代表协议与技术）首发于2015年，基于IPFS而建立的激励层: Filecoin(代表激励层的分布式存储网络)。


| Filecoin | IPFS | IPLD | libp2p |
| -------- | ---- | ---- | ------ |
| 价值数据  | 应用数据 | 定义数据 | 传递数据 |
| 激励/证券化 | 应用 | 对象/文件 | 路由/网路 |


# What is Filecoin Mining

Filecoin是存储挖矿，矿工根据其实际存储了多少数据并向链上提交了复制证明从而获得有效算力(有效存力)，有效算力越高，矿工获得区块奖励的概率越大。

算力越大的矿工，获得区块打包的权利或者概率越大，这里就有赢票率和出块率的参数。赢票率就是赢得选票的概率，出块率就是获得区块奖励的概率。

与传统PoW不一样的是: 

```
1. Filecoin网络的角色更多，提供数据存储或数据检索都可以获得收益
2. Filecoin有抵押挖矿机制，通过抵押代币来约束矿工保持长期存储
3. 获得区块奖励也会有一部分放入锁定的资金，完成存储合同后才获得所有奖励
```


# Filecoin Mining Process


![挖矿过程](1.png)

相关名词

- 数据封存：封存客户提交的数据。须经过precommit和commit的过程(后续分析)。

- 复制证明：(Proof-of-Replication, 简称PoRep)，挖矿者的自证。

- 有效存力：经网络验证的有效存储。

- 赢票率：赢得打包出块权的概率。

- 时空证明：(Proofs-of-Spacetime，简称PoST)，挖矿者被验证。


Tips: 复制证明和时空证明是FIlecoin的两个关键共识机制。


需要注意的是: 

1. 生成复制证明获得算力的过程与提交时空证明参与区块打包是两个独立的过程。
2. 一般来说有效算力越大，占全网算力占比越高，则赢票率(获得打包权)越大，赢票率越大则出块率越高，获得区块奖励也就越多。
3. 但是有效算力占比和出块率并不是绝对对应的，当矿工的有效算力达到一定量级的时候，出块才会相对稳定。 


# How The Hardware Works

挖矿主要配置的硬件构成如下: 

| 硬件类型 | 硬件作用 | 选型要点 |
|--------|---------|--------|
| CPU | 更快的完成数据封装 | 仅考虑CPU的单一方案下，具备Hash计算指令的AMD处理器相对更优 |
| RAM | 封装32G/64G固定大小扇区需要相应的RAM容量 | 扇区越大所需RAM越大|
| GPU | 在一定的时间内完成零知识证明 | Lotus网络中只支持N卡 |
| 硬盘 | 存储文件/区块数据 | 需要稳定存储，预留空间提供查询服务，参与主网至少100TB容量 |
| 网络 | 作用于机器封装时与存储矿工的通讯 | 10GbE+网卡和交换机 |

挖矿步骤与资源使用的情况如下: 

1. 复制证明 PoRep

| 步骤详情| 任务详情 | 工作硬件| 预期时间 | 选型要点 |
|--------|--------|--------|---------|--------|
| Precommit-P1 | Single thread PoRep SDR encode | CPU | 几个小时 | 1. 取决于扇区大小和速度  2. AMD处理器SHA拓展可显著提高速度，或用Intel CPU+FPGA(算法定制)加速  3. 提高主频可提升速度 |
| Precommit-P2 | Poseidon Hash -> Mercle tree | GPU | 45-60mins | 可用CPU替换但会显著拖慢速度，可用FPGA(算法定制)显著加速 |
| Commit-C1 | generate prove's ready | CPU | 几十秒 | - |
| Commit-C2 | generate zk-SNARK and broadcast to other nodes | GPU | 20-30mins | 可用FPGA(算法定制)显著加速 |


2. 时空证明 PoSt

| 步骤详情| 任务详情 | 工作硬件| 预期时间 | 选型要点 |
|--------|--------|--------|---------|--------|
| Window-PoSt | 审核旷工的存储承诺 | GPU | 30mins内(24小时至少1次/每个扇区) | 多核CPU可加速，8核勉强过关，24核轻松过关 |
| Winning-PoSt | 在每一个epoch提交存力证明以获得挖出区块的机会 | GPU | 1个epoch内完成，目前为25s | 不需太多计算 |




Tips:
```
Filecoin第二阶段的Lotus测试网文件给出了“测试网+小规模”最低配置：
2TB硬盘空间，8核CPU、128G的内存..
```





# How The Software Works

| 节点软件 | 编程语言 |
|--------|---------|
|lotus | Go |
| forest | Rust |
| fuhon | C++ |
| go-filecoin | Go |

Tips:
```
- Golang开发的lotus项目的节点各项功能较为完善，一般使用这套来运行矿工节点。
- 算法部分的代码主要用Rust语言实现。
```

<p align="right">End.</p>

