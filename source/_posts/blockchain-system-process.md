---
title: 区块链技术框架发展：比特币&以太坊&HyperledgerFabric
top: false
cover: false
toc: true
mathjax: true
date: 2020-03-16 21:16:02
password:
summary: 整理一下目前区块链主流的技术架构表格(比特币&以太坊&HyperledgerFabric)。
tags:
    - 区块链
    - BTC
    - Ethereum
    - Fabric
categories:
    - 区块链
---



# 区块链1.0架构 比特币系统(2009)

产生概念：

- 去中心电子记账系统/分布式账本
- 密码学
- 挖矿/共识机制
 

# 区块链2.0架构 以太坊(2013)

产生概念(上一代基础上新增/改进)：

- 智能合约/EVM虚拟机/Gas
- 不同类型帐号(外部账户/合约账户)
- 改进的PoW共识
 

# 区块链3.0架构 Hyperledger及其他除货币以外的领域(2015)

产生概念(上一代基础上新增/改进)：

- 成员管理，接入认证
- 可拔插共识机制：PBFT/Paxos/Raft
- 分区/channel
- 更好的性能与扩展性


![区块链主流技术框架](1.png)



