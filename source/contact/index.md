---
title: contact
date: 2019-07-26 17:17:02
type: "contact"
layout: "contact"
---

# 欢迎留言

大家有问题可以Send Email联系我。

- email: kumataahh@gmail.com




# 友链交换

想要交换友链的小伙伴，欢迎Send Email给我留言，留言格式：

* **名称：**你的博客名称
* **地址：**你的博客地址
* **简介：**一句话简介
* **头像：**你的头像地址

例如我的博客友链，大家可以加到自己博客里哦：

* **名称：**kumataahh.github.io
* **地址：**https://kumataahh.github.io.com
* **简介：**[Kumataの区块研学室]这里会研究并分享有趣的区块链学习内容。
* **头像：**https://kumataahh.github.io.com/medias/avatars/kumata.jpg